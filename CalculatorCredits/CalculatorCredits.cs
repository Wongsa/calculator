﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorCredit
{
     public class CalculatorCredits
    {
       
        //เงินสดที่เหลือ
        public double CashOver(int cash, double downPayment)
        {
            return Math.Round(cash - downPayment, 2, MidpointRounding.AwayFromZero);
        }

        //ดอกเบี้ยเช่าซื้อทั้งหมด
        public double TotalInterest(double cashOver, double interest, int numPeriods)
        {
            return Math.Round((cashOver * (interest/100.0)) * (numPeriods / 12.0) ,2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อรวมดอก
        public double Rent(double cashOver, double totalInterest)
        {
            return Math.Round(cashOver + totalInterest, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อ/เดือน
        public double LeasePerMonth(double rent, double numPeriods)
        {
            return Math.Round(rent / numPeriods ,2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อส่วนลด
        public double LeaseDiscountPerMonth(double cashOver, double interestOnTime, int numPeriods)
        {
            double leaseDiscount = (cashOver * (interestOnTime / 100.0)) * (numPeriods / 12.0);
            double leaseDiscountPerMonth = (leaseDiscount + cashOver) / numPeriods;

            return Math.Round(leaseDiscountPerMonth, 2, MidpointRounding.AwayFromZero);
        }

        //ส่วนลด
        public double DiscountPerMonth(double leasePerMonth, double leaseDiscountPerMonth)
        {
            return Math.Round(leasePerMonth - leaseDiscountPerMonth, 2, MidpointRounding.AwayFromZero);
        }

        //ภาษี
        public double CalVat(double cashOver, double totalInterest)
        {
            return Math.Round((cashOver + totalInterest) * 0.07, 2, MidpointRounding.AwayFromZero);
        }

        //ภาษี/เดือน
        public double CalVatPerMonth(double calVat, double numPeriods)
        {
            return Math.Round(calVat / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ค่างวดต่อเดือน
        public double CalPeriodsMonth(double cashOver, double TotalInterest, double numPeriods)
        {
            return Math.Round((cashOver + TotalInterest) / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อส่วนลดVat
        public double CalDiscountVat(double cashOver, double interestOnTime, double numPeriods)
        {
            double leaseDiscount = (cashOver * (interestOnTime / 100.0)) * (numPeriods / 12.0);
            double Vat = (cashOver + leaseDiscount) * 0.07;
            double discountVat = (leaseDiscount + cashOver + Vat) / numPeriods;

            return Math.Round(discountVat, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อรวมดอกVat
        public double RentVat(double cashOver, double totalInterest, double calVat)
        {
            return Math.Round(cashOver + totalInterest + calVat, 2, MidpointRounding.AwayFromZero);
        }

        //ค่าเช่าซื้อ/เดือนVat
        public double LeasePerMonthVat(double rentVat, double numPeriods)
        {
            return Math.Round(rentVat / numPeriods, 2, MidpointRounding.AwayFromZero);
        }

        //ส่วนลดVat
        public double DiscountPerMonthVat(double leasePerMonthVat, double discountVat)
        {
            return Math.Round(leasePerMonthVat - discountVat, 2, MidpointRounding.AwayFromZero);
        }

        //วันเกิด
        public int CalAge(int birthday)
        {
            return DateTime.Today.Year - birthday;
        }
    }
}
