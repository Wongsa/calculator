﻿namespace Payment
{
    partial class frmCraditCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculate = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLeasePerMonth = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDiscountPerMonth = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLeaseDiscountPerMonth = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInterestOnTime = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblInterestOnTime = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtVatPerMonth = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVat = new System.Windows.Forms.TextBox();
            this.lblVat = new System.Windows.Forms.Label();
            this.txtPeriodsMonth = new System.Windows.Forms.TextBox();
            this.txtRent = new System.Windows.Forms.TextBox();
            this.txtTotalInterest = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.lblInterrested = new System.Windows.Forms.Label();
            this.groupLeaseTerms = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.optLoan = new System.Windows.Forms.RadioButton();
            this.optLeasing = new System.Windows.Forms.RadioButton();
            this.txtTotalLease = new System.Windows.Forms.TextBox();
            this.lblTotalLease = new System.Windows.Forms.Label();
            this.txtCashLeftovers = new System.Windows.Forms.TextBox();
            this.txtNumPeriods = new System.Windows.Forms.TextBox();
            this.txtDownPayment = new System.Windows.Forms.TextBox();
            this.txtInterest = new System.Windows.Forms.TextBox();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.lblCashLeftovers = new System.Windows.Forms.Label();
            this.lblNumPeriods = new System.Windows.Forms.Label();
            this.lblInterest = new System.Windows.Forms.Label();
            this.lblDownPayment = new System.Windows.Forms.Label();
            this.lblCash = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboSex = new System.Windows.Forms.ComboBox();
            this.tmrBirthday = new System.Windows.Forms.DateTimePicker();
            this.optMTL = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.optNo = new System.Windows.Forms.RadioButton();
            this.optAIA = new System.Windows.Forms.RadioButton();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPremiumRate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInsurance = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupLeaseTerms.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCalculate
            // 
            this.btnCalculate.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCalculate.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnCalculate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculate.ForeColor = System.Drawing.Color.Black;
            this.btnCalculate.Location = new System.Drawing.Point(258, 316);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(86, 42);
            this.btnCalculate.TabIndex = 43;
            this.btnCalculate.Text = "คำนวณ";
            this.btnCalculate.UseVisualStyleBackColor = false;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(350, 136);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 18);
            this.label21.TabIndex = 34;
            this.label21.Text = "บาท";
            // 
            // txtLeasePerMonth
            // 
            this.txtLeasePerMonth.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtLeasePerMonth.Cursor = System.Windows.Forms.Cursors.No;
            this.txtLeasePerMonth.Enabled = false;
            this.txtLeasePerMonth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeasePerMonth.Location = new System.Drawing.Point(147, 272);
            this.txtLeasePerMonth.Name = "txtLeasePerMonth";
            this.txtLeasePerMonth.Size = new System.Drawing.Size(197, 26);
            this.txtLeasePerMonth.TabIndex = 10;
            this.txtLeasePerMonth.Text = "0.00";
            this.txtLeasePerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(48, 272);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 18);
            this.label13.TabIndex = 24;
            this.label13.Text = "ค่าเช่าซื้อ/เดือน";
            // 
            // txtDiscountPerMonth
            // 
            this.txtDiscountPerMonth.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtDiscountPerMonth.Cursor = System.Windows.Forms.Cursors.No;
            this.txtDiscountPerMonth.Enabled = false;
            this.txtDiscountPerMonth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountPerMonth.Location = new System.Drawing.Point(147, 356);
            this.txtDiscountPerMonth.Name = "txtDiscountPerMonth";
            this.txtDiscountPerMonth.Size = new System.Drawing.Size(197, 26);
            this.txtDiscountPerMonth.TabIndex = 11;
            this.txtDiscountPerMonth.Text = "0.00";
            this.txtDiscountPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(62, 356);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 18);
            this.label12.TabIndex = 22;
            this.label12.Text = "ส่วนลด/เดือน";
            // 
            // txtLeaseDiscountPerMonth
            // 
            this.txtLeaseDiscountPerMonth.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtLeaseDiscountPerMonth.Cursor = System.Windows.Forms.Cursors.No;
            this.txtLeaseDiscountPerMonth.Enabled = false;
            this.txtLeaseDiscountPerMonth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeaseDiscountPerMonth.Location = new System.Drawing.Point(147, 405);
            this.txtLeaseDiscountPerMonth.Name = "txtLeaseDiscountPerMonth";
            this.txtLeaseDiscountPerMonth.Size = new System.Drawing.Size(197, 26);
            this.txtLeaseDiscountPerMonth.TabIndex = 12;
            this.txtLeaseDiscountPerMonth.Text = "0.00";
            this.txtLeaseDiscountPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 407);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 18);
            this.label11.TabIndex = 20;
            this.label11.Text = "ค่าเช่าส่วนลด/เดือน";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Orange;
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnRefresh.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Black;
            this.btnRefresh.Location = new System.Drawing.Point(140, 316);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(80, 42);
            this.btnRefresh.TabIndex = 42;
            this.btnRefresh.Text = "คืนค่า";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(359, 135);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 18);
            this.label19.TabIndex = 33;
            this.label19.Text = "บาท";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(359, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 18);
            this.label18.TabIndex = 30;
            this.label18.Text = "เดือน";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(359, 238);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 18);
            this.label16.TabIndex = 28;
            this.label16.Text = "%ต่อปี";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(359, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 18);
            this.label14.TabIndex = 26;
            this.label14.Text = "บาท";
            // 
            // txtInterestOnTime
            // 
            this.txtInterestOnTime.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtInterestOnTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtInterestOnTime.Location = new System.Drawing.Point(156, 271);
            this.txtInterestOnTime.Name = "txtInterestOnTime";
            this.txtInterestOnTime.Size = new System.Drawing.Size(197, 26);
            this.txtInterestOnTime.TabIndex = 4;
            this.txtInterestOnTime.Text = "12";
            this.txtInterestOnTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInterestOnTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestOnTime_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(350, 32);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 18);
            this.label26.TabIndex = 39;
            this.label26.Text = "บาท";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(350, 411);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 18);
            this.label25.TabIndex = 38;
            this.label25.Text = "บาท";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(350, 362);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 18);
            this.label24.TabIndex = 37;
            this.label24.Text = "บาท";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(350, 278);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 18);
            this.label23.TabIndex = 36;
            this.label23.Text = "บาท";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(359, 277);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 18);
            this.label15.TabIndex = 41;
            this.label15.Text = "%ต่อปี";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(359, 203);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 18);
            this.label27.TabIndex = 40;
            this.label27.Text = "บาท";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(350, 181);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 18);
            this.label22.TabIndex = 35;
            this.label22.Text = "บาท";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(359, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 18);
            this.label20.TabIndex = 32;
            this.label20.Text = "บาท";
            // 
            // lblInterestOnTime
            // 
            this.lblInterestOnTime.AutoSize = true;
            this.lblInterestOnTime.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterestOnTime.Location = new System.Drawing.Point(24, 276);
            this.lblInterestOnTime.Name = "lblInterestOnTime";
            this.lblInterestOnTime.Size = new System.Drawing.Size(118, 18);
            this.lblInterestOnTime.TabIndex = 18;
            this.lblInterestOnTime.Text = "ดอกเบี้ยเช่าซื้อตรงเวลา";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Linen;
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.txtVatPerMonth);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtVat);
            this.groupBox2.Controls.Add(this.lblVat);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtLeasePerMonth);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtDiscountPerMonth);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtLeaseDiscountPerMonth);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtPeriodsMonth);
            this.groupBox2.Controls.Add(this.txtRent);
            this.groupBox2.Controls.Add(this.txtTotalInterest);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.lbl);
            this.groupBox2.Controls.Add(this.lblInterrested);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.No;
            this.groupBox2.Location = new System.Drawing.Point(467, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(403, 636);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(350, 226);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 18);
            this.label34.TabIndex = 45;
            this.label34.Text = "บาท";
            // 
            // txtVatPerMonth
            // 
            this.txtVatPerMonth.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtVatPerMonth.Cursor = System.Windows.Forms.Cursors.No;
            this.txtVatPerMonth.Enabled = false;
            this.txtVatPerMonth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVatPerMonth.Location = new System.Drawing.Point(147, 220);
            this.txtVatPerMonth.Name = "txtVatPerMonth";
            this.txtVatPerMonth.Size = new System.Drawing.Size(197, 26);
            this.txtVatPerMonth.TabIndex = 44;
            this.txtVatPerMonth.Text = "0.00";
            this.txtVatPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(62, 220);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 18);
            this.label35.TabIndex = 43;
            this.label35.Text = "ค่าภาษี/เดือน\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(350, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 18);
            this.label2.TabIndex = 42;
            this.label2.Text = "บาท";
            // 
            // txtVat
            // 
            this.txtVat.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtVat.Cursor = System.Windows.Forms.Cursors.No;
            this.txtVat.Enabled = false;
            this.txtVat.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVat.Location = new System.Drawing.Point(147, 78);
            this.txtVat.Name = "txtVat";
            this.txtVat.Size = new System.Drawing.Size(197, 26);
            this.txtVat.TabIndex = 41;
            this.txtVat.Text = "0.00";
            this.txtVat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVat
            // 
            this.lblVat.AutoSize = true;
            this.lblVat.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVat.Location = new System.Drawing.Point(78, 78);
            this.lblVat.Name = "lblVat";
            this.lblVat.Size = new System.Drawing.Size(53, 19);
            this.lblVat.TabIndex = 40;
            this.lblVat.Text = "Vat 7%";
            // 
            // txtPeriodsMonth
            // 
            this.txtPeriodsMonth.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtPeriodsMonth.Cursor = System.Windows.Forms.Cursors.No;
            this.txtPeriodsMonth.Enabled = false;
            this.txtPeriodsMonth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriodsMonth.Location = new System.Drawing.Point(147, 175);
            this.txtPeriodsMonth.Name = "txtPeriodsMonth";
            this.txtPeriodsMonth.Size = new System.Drawing.Size(197, 26);
            this.txtPeriodsMonth.TabIndex = 9;
            this.txtPeriodsMonth.Text = "0.00";
            this.txtPeriodsMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRent
            // 
            this.txtRent.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtRent.Cursor = System.Windows.Forms.Cursors.No;
            this.txtRent.Enabled = false;
            this.txtRent.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRent.Location = new System.Drawing.Point(147, 130);
            this.txtRent.Name = "txtRent";
            this.txtRent.Size = new System.Drawing.Size(197, 26);
            this.txtRent.TabIndex = 8;
            this.txtRent.Text = "0.00";
            this.txtRent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalInterest
            // 
            this.txtTotalInterest.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtTotalInterest.Cursor = System.Windows.Forms.Cursors.No;
            this.txtTotalInterest.Enabled = false;
            this.txtTotalInterest.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInterest.Location = new System.Drawing.Point(147, 26);
            this.txtTotalInterest.Name = "txtTotalInterest";
            this.txtTotalInterest.Size = new System.Drawing.Size(197, 26);
            this.txtTotalInterest.TabIndex = 7;
            this.txtTotalInterest.Text = "0.00";
            this.txtTotalInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(64, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "ค่างวด/เดือน";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(21, 130);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(110, 18);
            this.lbl.TabIndex = 6;
            this.lbl.Text = "ค่าเช่าซื้อรวมดอกเบี้ย";
            // 
            // lblInterrested
            // 
            this.lblInterrested.AutoSize = true;
            this.lblInterrested.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterrested.Location = new System.Drawing.Point(18, 26);
            this.lblInterrested.Name = "lblInterrested";
            this.lblInterrested.Size = new System.Drawing.Size(113, 18);
            this.lblInterrested.TabIndex = 5;
            this.lblInterrested.Text = "ดอกเบี้ยเช่าซื้อทั้งหมด";
            // 
            // groupLeaseTerms
            // 
            this.groupLeaseTerms.BackColor = System.Drawing.Color.Linen;
            this.groupLeaseTerms.Controls.Add(this.label1);
            this.groupLeaseTerms.Controls.Add(this.optLoan);
            this.groupLeaseTerms.Controls.Add(this.optLeasing);
            this.groupLeaseTerms.Controls.Add(this.btnCalculate);
            this.groupLeaseTerms.Controls.Add(this.btnRefresh);
            this.groupLeaseTerms.Controls.Add(this.label15);
            this.groupLeaseTerms.Controls.Add(this.label27);
            this.groupLeaseTerms.Controls.Add(this.label19);
            this.groupLeaseTerms.Controls.Add(this.label20);
            this.groupLeaseTerms.Controls.Add(this.label18);
            this.groupLeaseTerms.Controls.Add(this.label16);
            this.groupLeaseTerms.Controls.Add(this.label14);
            this.groupLeaseTerms.Controls.Add(this.txtInterestOnTime);
            this.groupLeaseTerms.Controls.Add(this.lblInterestOnTime);
            this.groupLeaseTerms.Controls.Add(this.txtTotalLease);
            this.groupLeaseTerms.Controls.Add(this.lblTotalLease);
            this.groupLeaseTerms.Controls.Add(this.txtCashLeftovers);
            this.groupLeaseTerms.Controls.Add(this.txtNumPeriods);
            this.groupLeaseTerms.Controls.Add(this.txtDownPayment);
            this.groupLeaseTerms.Controls.Add(this.txtInterest);
            this.groupLeaseTerms.Controls.Add(this.txtCash);
            this.groupLeaseTerms.Controls.Add(this.lblCashLeftovers);
            this.groupLeaseTerms.Controls.Add(this.lblNumPeriods);
            this.groupLeaseTerms.Controls.Add(this.lblInterest);
            this.groupLeaseTerms.Controls.Add(this.lblDownPayment);
            this.groupLeaseTerms.Controls.Add(this.lblCash);
            this.groupLeaseTerms.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupLeaseTerms.Location = new System.Drawing.Point(13, 15);
            this.groupLeaseTerms.Name = "groupLeaseTerms";
            this.groupLeaseTerms.Size = new System.Drawing.Size(444, 368);
            this.groupLeaseTerms.TabIndex = 44;
            this.groupLeaseTerms.TabStop = false;
            this.groupLeaseTerms.Text = "เงื่อนไขเช่าซื้อ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(58, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 46;
            this.label1.Text = "ประเภทสัญญา";
            // 
            // optLoan
            // 
            this.optLoan.AutoSize = true;
            this.optLoan.Checked = true;
            this.optLoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.optLoan.Location = new System.Drawing.Point(156, 28);
            this.optLoan.Name = "optLoan";
            this.optLoan.Size = new System.Drawing.Size(53, 22);
            this.optLoan.TabIndex = 45;
            this.optLoan.TabStop = true;
            this.optLoan.Text = "เงินกู้";
            this.optLoan.UseVisualStyleBackColor = true;
            // 
            // optLeasing
            // 
            this.optLeasing.AutoSize = true;
            this.optLeasing.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.optLeasing.Location = new System.Drawing.Point(212, 28);
            this.optLeasing.Name = "optLeasing";
            this.optLeasing.Size = new System.Drawing.Size(76, 22);
            this.optLeasing.TabIndex = 44;
            this.optLeasing.Text = "เช่าซื้อรถ";
            this.optLeasing.UseVisualStyleBackColor = true;
            // 
            // txtTotalLease
            // 
            this.txtTotalLease.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtTotalLease.Cursor = System.Windows.Forms.Cursors.No;
            this.txtTotalLease.Enabled = false;
            this.txtTotalLease.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalLease.Location = new System.Drawing.Point(156, 197);
            this.txtTotalLease.Name = "txtTotalLease";
            this.txtTotalLease.Size = new System.Drawing.Size(197, 26);
            this.txtTotalLease.TabIndex = 6;
            this.txtTotalLease.Text = "0.00";
            this.txtTotalLease.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalLease
            // 
            this.lblTotalLease.AutoSize = true;
            this.lblTotalLease.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalLease.Location = new System.Drawing.Point(61, 201);
            this.lblTotalLease.Name = "lblTotalLease";
            this.lblTotalLease.Size = new System.Drawing.Size(81, 18);
            this.lblTotalLease.TabIndex = 16;
            this.lblTotalLease.Text = "ค่าเช่าซื้อทั้งสิ้น";
            // 
            // txtCashLeftovers
            // 
            this.txtCashLeftovers.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtCashLeftovers.Cursor = System.Windows.Forms.Cursors.No;
            this.txtCashLeftovers.Enabled = false;
            this.txtCashLeftovers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashLeftovers.Location = new System.Drawing.Point(156, 129);
            this.txtCashLeftovers.Name = "txtCashLeftovers";
            this.txtCashLeftovers.Size = new System.Drawing.Size(197, 26);
            this.txtCashLeftovers.TabIndex = 5;
            this.txtCashLeftovers.Text = "0.00";
            this.txtCashLeftovers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNumPeriods
            // 
            this.txtNumPeriods.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtNumPeriods.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtNumPeriods.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtNumPeriods.Location = new System.Drawing.Point(156, 161);
            this.txtNumPeriods.Name = "txtNumPeriods";
            this.txtNumPeriods.Size = new System.Drawing.Size(197, 26);
            this.txtNumPeriods.TabIndex = 2;
            this.txtNumPeriods.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumPeriods.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumPeriods_KeyPress);
            // 
            // txtDownPayment
            // 
            this.txtDownPayment.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDownPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtDownPayment.Location = new System.Drawing.Point(156, 95);
            this.txtDownPayment.Name = "txtDownPayment";
            this.txtDownPayment.Size = new System.Drawing.Size(197, 26);
            this.txtDownPayment.TabIndex = 1;
            this.txtDownPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDownPayment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDownPayment_KeyPress);
            this.txtDownPayment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDownPayment_KeyUp);
            this.txtDownPayment.Leave += new System.EventHandler(this.txtDownPayment_Leave);
            // 
            // txtInterest
            // 
            this.txtInterest.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtInterest.Location = new System.Drawing.Point(157, 235);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(197, 26);
            this.txtInterest.TabIndex = 3;
            this.txtInterest.Text = "15";
            this.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInterest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterest_KeyPress);
            // 
            // txtCash
            // 
            this.txtCash.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtCash.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCash.Location = new System.Drawing.Point(156, 60);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(197, 26);
            this.txtCash.TabIndex = 0;
            this.txtCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCash_KeyPress);
            this.txtCash.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCash_KeyUp);
            this.txtCash.Leave += new System.EventHandler(this.txtCash_Leave);
            // 
            // lblCashLeftovers
            // 
            this.lblCashLeftovers.AutoSize = true;
            this.lblCashLeftovers.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashLeftovers.Location = new System.Drawing.Point(50, 133);
            this.lblCashLeftovers.Name = "lblCashLeftovers";
            this.lblCashLeftovers.Size = new System.Drawing.Size(92, 18);
            this.lblCashLeftovers.TabIndex = 4;
            this.lblCashLeftovers.Text = "เงินสดส่วนที่เหลือ";
            // 
            // lblNumPeriods
            // 
            this.lblNumPeriods.AutoSize = true;
            this.lblNumPeriods.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumPeriods.Location = new System.Drawing.Point(82, 165);
            this.lblNumPeriods.Name = "lblNumPeriods";
            this.lblNumPeriods.Size = new System.Drawing.Size(61, 18);
            this.lblNumPeriods.TabIndex = 3;
            this.lblNumPeriods.Text = "จำนวนงวด";
            // 
            // lblInterest
            // 
            this.lblInterest.AutoSize = true;
            this.lblInterest.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterest.Location = new System.Drawing.Point(65, 239);
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Size = new System.Drawing.Size(78, 18);
            this.lblInterest.TabIndex = 2;
            this.lblInterest.Text = "ดอกเบี้ยเช่าซื้อ";
            // 
            // lblDownPayment
            // 
            this.lblDownPayment.AutoSize = true;
            this.lblDownPayment.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownPayment.Location = new System.Drawing.Point(91, 101);
            this.lblDownPayment.Name = "lblDownPayment";
            this.lblDownPayment.Size = new System.Drawing.Size(51, 18);
            this.lblDownPayment.TabIndex = 1;
            this.lblDownPayment.Text = "เงินดาวน์";
            // 
            // lblCash
            // 
            this.lblCash.AutoSize = true;
            this.lblCash.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCash.Location = new System.Drawing.Point(106, 63);
            this.lblCash.Name = "lblCash";
            this.lblCash.Size = new System.Drawing.Size(38, 18);
            this.lblCash.TabIndex = 0;
            this.lblCash.Text = "เงินสด";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Linen;
            this.groupBox1.Controls.Add(this.cboSex);
            this.groupBox1.Controls.Add(this.tmrBirthday);
            this.groupBox1.Controls.Add(this.optMTL);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.optNo);
            this.groupBox1.Controls.Add(this.optAIA);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.txtAge);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtPremiumRate);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtInsurance);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCompany);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(13, 396);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 257);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "อัตราเบี้ยประกัน";
            // 
            // cboSex
            // 
            this.cboSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cboSex.FormattingEnabled = true;
            this.cboSex.Items.AddRange(new object[] {
            "ชาย",
            "หญิง"});
            this.cboSex.Location = new System.Drawing.Point(156, 155);
            this.cboSex.Name = "cboSex";
            this.cboSex.Size = new System.Drawing.Size(197, 26);
            this.cboSex.TabIndex = 52;
            this.cboSex.Text = "----------------เลือก-----------------";
            // 
            // tmrBirthday
            // 
            this.tmrBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tmrBirthday.Location = new System.Drawing.Point(156, 93);
            this.tmrBirthday.Name = "tmrBirthday";
            this.tmrBirthday.Size = new System.Drawing.Size(200, 22);
            this.tmrBirthday.TabIndex = 51;
            this.tmrBirthday.ValueChanged += new System.EventHandler(this.tmrBirthday_ValueChanged);
            // 
            // optMTL
            // 
            this.optMTL.AutoSize = true;
            this.optMTL.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optMTL.Location = new System.Drawing.Point(304, 29);
            this.optMTL.Name = "optMTL";
            this.optMTL.Size = new System.Drawing.Size(55, 23);
            this.optMTL.TabIndex = 50;
            this.optMTL.Text = "MTL";
            this.optMTL.UseVisualStyleBackColor = true;
            this.optMTL.MouseClick += new System.Windows.Forms.MouseEventHandler(this.optMTL_MouseClick);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(80, 27);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 18);
            this.label33.TabIndex = 49;
            this.label33.Text = "เบี้ยประกัน\r\n";
            // 
            // optNo
            // 
            this.optNo.AutoSize = true;
            this.optNo.Checked = true;
            this.optNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optNo.Location = new System.Drawing.Point(157, 29);
            this.optNo.Name = "optNo";
            this.optNo.Size = new System.Drawing.Size(84, 22);
            this.optNo.TabIndex = 48;
            this.optNo.TabStop = true;
            this.optNo.Text = "ไม่ทำประกัน";
            this.optNo.UseVisualStyleBackColor = true;
            this.optNo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.optNo_MouseClick);
            // 
            // optAIA
            // 
            this.optAIA.AutoSize = true;
            this.optAIA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAIA.Location = new System.Drawing.Point(250, 29);
            this.optAIA.Name = "optAIA";
            this.optAIA.Size = new System.Drawing.Size(49, 23);
            this.optAIA.TabIndex = 47;
            this.optAIA.Text = "AIA";
            this.optAIA.UseVisualStyleBackColor = true;
            this.optAIA.MouseClick += new System.Windows.Forms.MouseEventHandler(this.optAIA_MouseClick);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(98, 93);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 18);
            this.label32.TabIndex = 43;
            this.label32.Text = "วันเกิด";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(360, 127);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(15, 18);
            this.label29.TabIndex = 41;
            this.label29.Text = "ปี";
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAge.Location = new System.Drawing.Point(157, 121);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(197, 26);
            this.txtAge.TabIndex = 39;
            this.txtAge.Text = "----------------อายุ-----------------";
            this.txtAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(109, 127);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(28, 18);
            this.label30.TabIndex = 40;
            this.label30.Text = "อายุ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(360, 222);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 18);
            this.label17.TabIndex = 38;
            this.label17.Text = "บาท";
            // 
            // txtPremiumRate
            // 
            this.txtPremiumRate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtPremiumRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPremiumRate.Location = new System.Drawing.Point(157, 187);
            this.txtPremiumRate.Name = "txtPremiumRate";
            this.txtPremiumRate.Size = new System.Drawing.Size(197, 26);
            this.txtPremiumRate.TabIndex = 36;
            this.txtPremiumRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(67, 222);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(70, 18);
            this.label28.TabIndex = 37;
            this.label28.Text = "ค่าเบี้ยประกัน";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(360, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 18);
            this.label9.TabIndex = 35;
            this.label9.Text = "%";
            // 
            // txtInsurance
            // 
            this.txtInsurance.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtInsurance.Location = new System.Drawing.Point(157, 219);
            this.txtInsurance.Name = "txtInsurance";
            this.txtInsurance.Size = new System.Drawing.Size(197, 26);
            this.txtInsurance.TabIndex = 33;
            this.txtInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(54, 192);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 18);
            this.label10.TabIndex = 34;
            this.label10.Text = "อัตราเบี้ยประกัน";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(360, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 20);
            this.label4.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(109, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 18);
            this.label5.TabIndex = 31;
            this.label5.Text = "เพศ";
            // 
            // txtCompany
            // 
            this.txtCompany.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtCompany.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompany.Location = new System.Drawing.Point(157, 55);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(197, 29);
            this.txtCompany.TabIndex = 27;
            this.txtCompany.Text = "ไม่ทำประกัน";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 18);
            this.label3.TabIndex = 28;
            this.label3.Text = "บริษัทประกัน";
            // 
            // frmCraditCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.Bisque;
            this.ClientSize = new System.Drawing.Size(890, 668);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupLeaseTerms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCraditCal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "คำนวณค่างวด";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupLeaseTerms.ResumeLayout(false);
            this.groupLeaseTerms.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtLeasePerMonth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDiscountPerMonth;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtLeaseDiscountPerMonth;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtInterestOnTime;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblInterestOnTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPeriodsMonth;
        private System.Windows.Forms.TextBox txtRent;
        private System.Windows.Forms.TextBox txtTotalInterest;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label lblInterrested;
        private System.Windows.Forms.GroupBox groupLeaseTerms;
        private System.Windows.Forms.TextBox txtTotalLease;
        private System.Windows.Forms.Label lblTotalLease;
        private System.Windows.Forms.TextBox txtCashLeftovers;
        private System.Windows.Forms.TextBox txtNumPeriods;
        private System.Windows.Forms.TextBox txtDownPayment;
        private System.Windows.Forms.TextBox txtInterest;
        private System.Windows.Forms.TextBox txtCash;
        private System.Windows.Forms.Label lblCashLeftovers;
        private System.Windows.Forms.Label lblNumPeriods;
        private System.Windows.Forms.Label lblInterest;
        private System.Windows.Forms.Label lblDownPayment;
        private System.Windows.Forms.Label lblCash;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton optLoan;
        private System.Windows.Forms.RadioButton optLeasing;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optMTL;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.RadioButton optNo;
        private System.Windows.Forms.RadioButton optAIA;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPremiumRate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInsurance;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker tmrBirthday;
        private System.Windows.Forms.ComboBox cboSex;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtVatPerMonth;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVat;
        private System.Windows.Forms.Label lblVat;
    }
}