﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CalculatorCredit;

namespace Payment
{
    public partial class frmCraditCal : Form
    {
        public frmCraditCal()
        {
            InitializeComponent();
        }

        private int m_cash = 0;
        private double m_downPayment = 0;
        private int m_numPeriods = 0;
        private double m_interest = 0;
        private double m_interestOnTime = 0;
        private int m_dot;
        private int m_numDouble;

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (IsValidate())
            {
               
            }
            else
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ");
            }
        }

        private bool IsValidate()
        {
            if (txtCash.Text != "" && txtDownPayment.Text != "" && txtNumPeriods.Text != ""
                && txtInterest.Text != "" && txtInterestOnTime.Text != "")
            {
                m_cash = int.Parse(TxtReplace());
                m_downPayment = double.Parse(txtDownPayment.Text);
                m_numPeriods = int.Parse(txtNumPeriods.Text);
                m_interest = double.Parse(txtInterest.Text);
                m_interestOnTime = double.Parse(txtInterestOnTime.Text);

                return true;
            }
            return false;
        }

        public void CheckTxt(object sender, KeyPressEventArgs e)
        {

            //เช็ค เลข,dot,-
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;


            // พิมได้ 13 ตัว
            if ((sender as TextBox).Text.Length == 13 && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') == -1 && e.KeyChar != '.') ||
                ((sender as TextBox).Text.Length == 13 + m_numDouble + 3 && ((sender as TextBox).Text.IndexOf('.') > -1 && e.KeyChar == '.')))
            {
                e.Handled = true;
            }

            //เช็ค.ตัวเดียว
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }

            //เช็ค.ให้ทศนิยม
            if ((sender as TextBox).Text.EndsWith("."))
            {
                m_dot = (sender as TextBox).Text.Length;
                m_numDouble = 0;
            }

            // ทศนิยม 2 ตำแหน่งให้หยุดพิม
            if (m_numDouble >= 2 && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != 8 && (sender as TextBox).SelectionStart >= m_dot)
            {
                e.Handled = true;
            }

            //Curser>dot&deleteให้ทศนิยม-- 
            if ((sender as TextBox).SelectionStart >= m_dot && e.KeyChar == 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_numDouble--;
            }

            //Curser>dot&!delete ใหทศนิยม++ = 2
            if ((sender as TextBox).SelectionStart >= m_dot && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_numDouble++;
                if (m_numDouble >= 2)
                {
                    m_numDouble = 2;
                }
            }

            //Curser<dot&!deleteให้dot++
            if ((sender as TextBox).SelectionStart < m_dot && e.KeyChar != 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_dot++;
            }

            //Curser<dot&deleteให้dot-- =1
            if ((sender as TextBox).SelectionStart < m_dot && e.KeyChar == 8 && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                m_dot--;
                if (m_dot <= 1)
                {
                    m_dot = 1;
                }
            }
        }

        public void CheckComma(object sender, EventArgs e)
        {
            if ((sender as TextBox).Text != "")
            {
                (sender as TextBox).Text = string.Format("{0:n}", double.Parse((sender as TextBox).Text));
                (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtCash.RefreshCurrent();
            txtDownPayment.RefreshCurrent();
            txtCashLeftovers.RefreshCurrent();
            txtNumPeriods.RefreshCurrent();
            txtTotalLease.RefreshCurrent();
            txtTotalInterest.RefreshCurrent();
            txtRent.RefreshCurrent();
            txtLeasePerMonth.RefreshCurrent();
            txtLeaseDiscountPerMonth.RefreshCurrent();
            txtDiscountPerMonth.RefreshCurrent();
            txtVat.RefreshCurrent();
            txtPeriodsMonth.RefreshCurrent();
            txtVatPerMonth.RefreshCurrent();

        }

        

        private void AutoCaculate()
        {
            if (txtDownPayment.Text != "")
            {
                int m_cash = int.Parse(TxtReplace());
                double m_downPayment = double.Parse(txtDownPayment.Text);

                CalculatorCredits cal = new CalculatorCredits();
                double calCashOver = cal.CashOver(m_cash, m_downPayment);
                txtCashLeftovers.Text = calCashOver.ToString("#,###.00");
                txtTotalLease.Text = calCashOver.ToString("#,###.00");

            }
            else
            {
                txtCashLeftovers.Text = txtCash.Text;
                txtTotalLease.Text = txtCash.Text;
            }
        }

        private string TxtReplace()
        {
            string a = txtCash.Text.Replace(",", "");
            string b = a.Replace(".00", "");
            return b;
        }
        private void checkCompany(object sender, MouseEventArgs e)
        {
            if (optNo.Checked)
            {
                txtCompany.Text = "ไม่ทำประกัน";
                if (optLoan.Checked)
                {
                    Calculate();
                }
                else
                {
                    CalculateLeasing();
                }

            }
            else if (optAIA.Checked)
            {
                txtCompany.Text = "AIA";
                MessageBox.Show("eeeeeeeeee");
            }
            else
            {
                txtCompany.Text = "MTL";
                MessageBox.Show("ffffffffffff");
            }
        }

        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }

        private void txtDownPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }

        private void txtNumPeriods_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar=='0')
            //{
            //    e.Handled = true;
            //    CheckZero();
            //}
            CheckTxt(sender, e);
            
        }

        //private string CheckZero()
        //{

        //    if ((txtNumPeriods.Text != "0" || txtNumPeriods.Text != "" )&& txtNumPeriods.Text.Length > 1)
        //    {
               
        //        return txtNumPeriods.Text;
        //    }
        //    else if(txtNumPeriods.Text == "0" || txtNumPeriods.Text == "" )
        //    {
        //        txtNumPeriods.Text = "";
        //        MessageBox.Show("No");
        //    }
        //    return "";
        //}

        private void txtInterest_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }

        private void txtInterestOnTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckTxt(sender, e);
        }

        private void txtCash_Leave(object sender, EventArgs e)
        {
            CheckComma(sender, e);
        }

        private void txtDownPayment_Leave(object sender, EventArgs e)
        {
            CheckComma(sender, e);
        }

        private void optNo_MouseClick(object sender, MouseEventArgs e)
        {
            checkCompany(sender, e);
        }

        private void optAIA_MouseClick(object sender, MouseEventArgs e)
        {
            checkCompany(sender, e);
        }

        private void optMTL_MouseClick(object sender, MouseEventArgs e)
        {
            checkCompany(sender, e);
        }

        private void tmrBirthday_ValueChanged(object sender, EventArgs e)
        {
            CalculatorCredits cal = new CalculatorCredits();
            int birthday = tmrBirthday.Value.Year;
            int bDay = cal.CalAge(birthday);
            txtAge.Text = bDay.ToString();
            //int years = DateTime.Now.Year - dateTimePicker.Value.Year;
        }

        private void txtCash_KeyUp(object sender, KeyEventArgs e)
        {
            AutoCaculate();
        }
                 
        private void txtDownPayment_KeyUp(object sender, KeyEventArgs e)
        {
            AutoCaculate();
        }

        private void Calculate()
        {
            //เงินสดที่เหลือ
            CalculatorCredits cal = new CalculatorCredits();
            double calCashOver = cal.CashOver(m_cash, m_downPayment);
            txtCashLeftovers.Text = calCashOver.ToString("#,###.00");
            txtTotalLease.Text = calCashOver.ToString("#,###.00");

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            double totalInterest = cal.TotalInterest(calCashOver, m_interest, m_numPeriods);
            txtTotalInterest.Text = totalInterest.ToString("#,###.00");

            //ค่าเช่าซื้อรวมดอก
            double calRent = cal.Rent(calCashOver, totalInterest);
            txtRent.Text = calRent.ToString("#,###.00");

            //ค่าเช่าซื้อ/เดือน
            double leasePerMonth = cal.LeasePerMonth(calRent, m_numPeriods);
            txtLeasePerMonth.Text = leasePerMonth.ToString("#,###.00");

            //ค่าเช่าซื้อส่วนลด
            double leaseDiscountPerMonth = cal.LeaseDiscountPerMonth(calCashOver, m_interestOnTime, m_numPeriods);
            txtLeaseDiscountPerMonth.Text = leaseDiscountPerMonth.ToString("#,###.00");

            //ส่วนลดต่อเดือน
            double discountPerMonth = cal.DiscountPerMonth(leasePerMonth, leaseDiscountPerMonth);
            txtDiscountPerMonth.Text = discountPerMonth.ToString("#,###.00");
        }

        private void CalculateLeasing()
        {
            //เงินสดที่เหลือ
            CalculatorCredits cal = new CalculatorCredits();
            double calCashOver = cal.CashOver(m_cash, m_downPayment);
            txtCashLeftovers.Text = calCashOver.ToString("#,###.00");
            txtTotalLease.Text = calCashOver.ToString("#,###.00");

            //ดอกเบี้ยเช่าซื้อทั้งหมด
            double totalInterest = cal.TotalInterest(calCashOver, m_interest, m_numPeriods);
            txtTotalInterest.Text = totalInterest.ToString("#,###.00");

            //คำนวณภาษี
            double calVat = cal.CalVat(calCashOver, totalInterest);
            txtVat.Text = calVat.ToString("#,###.00");

            //ภาษี/เดือน
            double vatPerMonth = cal.CalVatPerMonth(calVat, m_numPeriods);
            txtVatPerMonth.Text = vatPerMonth.ToString("#,###.00");

            //ค่างวด/เดือน
            double calPeriodsMonth = cal.CalPeriodsMonth(calCashOver, totalInterest, m_numPeriods);
            txtPeriodsMonth.Text = calPeriodsMonth.ToString("#,###.00");

            //ค่าเช่าซื้อส่วนลด
            double discountVat = cal.CalDiscountVat(calCashOver, m_interestOnTime, m_numPeriods);
            txtLeaseDiscountPerMonth.Text = discountVat.ToString("#,###.00");

            //ค่าเช่าซื้อรวมดอกVat
            double calRentVat = cal.RentVat(calCashOver, totalInterest, calVat);
            txtRent.Text = calRentVat.ToString("#,###.00");

            ////ค่าเช่าซื้อต่อเดือนVat
            double leasePerMonthVat = cal.LeasePerMonth(calRentVat, m_numPeriods);
            txtLeasePerMonth.Text = leasePerMonthVat.ToString("#,###.00");

            ///ส่วนลดVat
            double discountPerMonthVat = cal.DiscountPerMonthVat(leasePerMonthVat, discountVat);
            txtDiscountPerMonth.Text = discountPerMonthVat.ToString("#,###.00");
        }


    }
}
